package tv.hooq.com.hooqtv.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import tv.hooq.com.hooqtv.R;


/**
 * Created by abdulaleem on 20/9/18.
 */

public class AnimationUtil {

    public static void startZoomInAnimation(View view) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.zoom_in_animation);
        view.startAnimation(animation);
    }
}