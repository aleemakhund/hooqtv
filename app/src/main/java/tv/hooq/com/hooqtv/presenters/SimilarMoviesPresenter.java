package tv.hooq.com.hooqtv.presenters;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.models.Error;
import tv.hooq.com.hooqtv.models.Success;
import tv.hooq.com.hooqtv.network.Callback;
import tv.hooq.com.hooqtv.network.RetrofitManager;
import tv.hooq.com.hooqtv.views.ISimilarMoviesView;

public class SimilarMoviesPresenter extends AbstractPresenter<ISimilarMoviesView> {

    private RetrofitManager mRetrofitManager;
    private int pageNumber = 1;
    private int mMovieId;

    public SimilarMoviesPresenter(RetrofitManager retrofitManager, int movieId) {
        mRetrofitManager = retrofitManager;
        mMovieId = movieId;
    }

    @Override
    public void init() {

        if (!isValidView()) {
            return;
        }

        getView().setLoadMoreListener(() -> {
            pageNumber++;
            getSimilarMoviesFromApi();
        });

        getSimilarMoviesFromApi();
    }

    private void getSimilarMoviesFromApi() {
        if (isValidView() && pageNumber == 1) {
            // show loading
        }

        mRetrofitManager.getSimilarMovies(pageNumber, mMovieId, new Callback<Success>() {
            @Override
            public void execute(Success result) {
                updateUI(result);
            }
        }, new Callback<Error>() {
            @Override
            public void execute(Error result) {
                updateUI(null);
            }
        });
    }

    private void updateUI(Success result) {
        if (!isValidView()) {
            return;
        }

        if (result == null) {
            pageNumber = pageNumber > 1 ? pageNumber-- : 1;
            getView().showUserMessageRetry(
                    getActivity().getString(R.string.an_error_occurred_please_retry),
                    v -> getSimilarMoviesFromApi());
        } else {

            if (result.getPage() >= result.getTotalPages() && getView() != null) {
                getView().disableLoadMore();
            }

            if (getView() != null) {
                getView().hideUserMessage();
                getView().setSimilarMovies(result.getResults());
            }
        }
    }
}
