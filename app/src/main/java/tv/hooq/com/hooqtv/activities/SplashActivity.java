package tv.hooq.com.hooqtv.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.utils.AnimationUtil;

/**
 * Created by abdulaleem on 20/9/18.
 */

public class SplashActivity extends AppCompatActivity {

    private View tvSplashLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash);

        tvSplashLogo = findViewById(R.id.tvSplashLogo);
        AnimationUtil.startZoomInAnimation(tvSplashLogo);

        /*Loads Next Activity after 3 seconds*/
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }, 2000);

    }
}