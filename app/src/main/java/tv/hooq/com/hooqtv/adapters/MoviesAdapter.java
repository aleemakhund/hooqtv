package tv.hooq.com.hooqtv.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.List;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.models.Movie;
import tv.hooq.com.hooqtv.viewholders.MoviesItemViewHolder;
import tv.hooq.com.hooqtv.views.LoadMoreRecyclerAdapter;
import tv.hooq.com.hooqtv.views.MovieClickListener;

public class MoviesAdapter extends LoadMoreRecyclerAdapter<MoviesItemViewHolder> {

    private List<Movie> mMovies;
    private WeakReference<MovieClickListener> mMovieClickListener;

    public MoviesAdapter(List<Movie> movies, MovieClickListener movieClickListener) {
        mMovies = movies;
        mMovieClickListener = new WeakReference<>(movieClickListener);
    }

    public void updateList(List<Movie> list) {
        addData(list);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolderItem(ViewGroup viewGroup, int viewType) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_movie_item, viewGroup, false);
        return new MoviesItemViewHolder(view);
    }

    @Override
    public int getItemViewTypeId(int position) {
        return position;
    }

    @Override
    public void onBindViewHolderItem(MoviesItemViewHolder viewHolder, int position) {
        Movie movie = mMovies.get(position);
        viewHolder.onBind(movie, mMovieClickListener);
    }

    @Override
    public List getData() {
        return mMovies;
    }

}
