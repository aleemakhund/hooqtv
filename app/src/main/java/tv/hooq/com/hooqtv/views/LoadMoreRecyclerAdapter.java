package tv.hooq.com.hooqtv.views;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tv.hooq.com.hooqtv.R;

/**
 * LoadMoreRecyclerAdapter.
 *
 * @param <VH> ViewHolder class for this Adapter
 */
public abstract class LoadMoreRecyclerAdapter<VH> extends RecyclerView.Adapter {

    public static final int VIEW_TYPE_LOADING_MORE = 999;

    private boolean loadingMore = false;

    /**
     * Add data to the current list.
     *
     * @param data new data to be added.
     */
    public void addData(List data) {
        if (null == data || data.isEmpty()) {
            return;
        }

        int startPosition = getData().size();
        this.getData().addAll(data);
        if (loadingMore) {
            startPosition -= 1;
        }
        this.notifyItemRangeInserted(startPosition, getData().size());
        removeLoadMore();
    }

    /**
     * @return current Data
     */
    public abstract List getData();

    @Override
    public final int getItemCount() {
        if (getData() != null) {
            return getData().size() + (loadingMore ? 1 : 0);
        }
        return 0;
    }

    /**
     * Adds loading more.
     */
    public void addLoadMore() {
        loadingMore = true;
        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                notifyItemInserted(getData().size());
            }
        };
        handler.post(r);
    }

    /**
     * Removes loading more.
     */
    public void removeLoadMore() {
        loadingMore = false;
        notifyDataSetChanged();
    }

    @Override
    public final RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {
        if (viewtype == VIEW_TYPE_LOADING_MORE) {
            return new LoadingViewHolder(LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_loading_recycler_view, viewGroup, false));
        } else {
            return onCreateViewHolderItem(viewGroup, viewtype);
        }
    }

    @Override
    public final void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (LoadingViewHolder.class.isInstance(viewHolder)) {
            //bind loading view
        } else {
            onBindViewHolderItem((VH) viewHolder, position);
        }
    }

    @Override
    public final int getItemViewType(int position) {
        if (position >= this.getData().size()) {
            return VIEW_TYPE_LOADING_MORE;
        } else {
            return getItemViewTypeId(position);
        }
    }

    /**
     * Returns type of item at given position.
     *
     * @param position of the item
     * @return type of item at given position
     */
    public abstract int getItemViewTypeId(int position);

    /**
     * To create ViewHolder item in inherited classes.
     *
     * @param viewGroup container view
     * @param viewType  type of the view
     * @return created ViewHolder object
     */
    public abstract RecyclerView.ViewHolder onCreateViewHolderItem(ViewGroup viewGroup, int viewType);

    /**
     * To bind data to the ViewHolder item in inherited classes.
     *
     * @param viewHolder ViewHolder item
     * @param position   position of the item
     */
    public abstract void onBindViewHolderItem(VH viewHolder, int position);

    /**
     * Loading ViewHolder class.
     */
    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        /**
         * Default constructor with itemView.
         *
         * @param itemView view object
         */
        public LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }

    /**
     * @return true if loading more
     */
    public boolean isLoadingMore() {
        return loadingMore;
    }
}