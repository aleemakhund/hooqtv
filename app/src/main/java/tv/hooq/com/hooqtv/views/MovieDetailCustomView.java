package tv.hooq.com.hooqtv.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tv.hooq.com.hooqtv.Constants;
import tv.hooq.com.hooqtv.ImageLoader;
import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.models.Movie;

public class MovieDetailCustomView extends LinearLayout {

    private ImageView ivPoster;
    private TextView tvMovieTitle;
    private TextView tvReleaseDate;
    private TextView tvDescription;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in.
     */
    public MovieDetailCustomView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file.
     *
     * @param context The Context the view is running in.
     * @param attrs   The attributes of the XML tag that is inflating the view.
     */
    public MovieDetailCustomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating.
     *
     * @param context      The Context the view is running in.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     */
    public MovieDetailCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_movie_detail, this, true);
        ivPoster = view.findViewById(R.id.iv_poster);
        tvMovieTitle = view.findViewById(R.id.tv_movie_title);
        tvReleaseDate = view.findViewById(R.id.tv_release_date);
        tvDescription = view.findViewById(R.id.tv_description);
    }

    public void setMovieDetail(Movie movie) {
        String url = Constants.IMAGES_PATH + Constants.W500 + movie.getPosterPath();
        ImageLoader.loadImage(url, ivPoster, true);

        tvMovieTitle.setText(movie.getTitle());

        tvReleaseDate.setText(movie.getReleaseDate());

        tvDescription.setText(movie.getOverView());
    }

}
