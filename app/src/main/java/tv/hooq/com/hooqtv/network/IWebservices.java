package tv.hooq.com.hooqtv.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IWebservices {

    @GET("movie/now_playing")
    Call<JsonObject> getNowPlayingMovies(
            @Query("api_key") String apiKey,
            @Query("page") int page,
            @Query("language") String language
    );

    @GET("movie/{movie_id}/similar")
    Call<JsonObject> getSimilarMovies(
            @Path("movie_id") int movieId,
            @Query("api_key") String apiKey,
            @Query("page") int page,
            @Query("language") String language
    );
}
