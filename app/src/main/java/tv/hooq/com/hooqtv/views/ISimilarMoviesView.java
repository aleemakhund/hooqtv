package tv.hooq.com.hooqtv.views;

import java.util.List;

import tv.hooq.com.hooqtv.models.Movie;

public interface ISimilarMoviesView extends BaseView {

    void setSimilarMovies(List<Movie> movies);

    /**
     * Sets load more listener.
     *
     * @param loadMoreListener listener
     */
    void setLoadMoreListener(LoadMoreRecyclerView.LoadMoreListener loadMoreListener);

    /**
     * Disables load more from Recycler view.
     */
    void disableLoadMore();
}
