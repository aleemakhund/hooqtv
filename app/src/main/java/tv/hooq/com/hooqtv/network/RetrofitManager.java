package tv.hooq.com.hooqtv.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tv.hooq.com.hooqtv.Constants;
import tv.hooq.com.hooqtv.models.Error;
import tv.hooq.com.hooqtv.models.Success;

public class RetrofitManager {

    private static RetrofitManager instance;

    //API Retrofit objects
    private OkHttpClient mApiOkHttpClient;
    private Retrofit mApiRetrofit;

    private IWebservices mApiService;


    private RetrofitManager() {
        initializeApiService();
    }

    /**
     * Returns the singleton instance of this class.
     *
     * @return singleton instance
     */
    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    private void initializeApiService() {

        mApiOkHttpClient = new OkHttpClient.Builder().build();
        mApiRetrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(mApiOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mApiService = mApiRetrofit.create(IWebservices.class);
    }


    public void getNowPlayingMovies(int pageNumber, Callback<Success> successCallback, Callback<Error> errorCallback) {

        Call<JsonObject> call = mApiService.getNowPlayingMovies(Constants.API_KEY, pageNumber, Constants.LANGUAGE);
        call.enqueue(new retrofit2.Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful() && response.body() != null) {
                    Success successResponse = new Gson().fromJson(response.body().toString(), Success.class);
                    successCallback.execute(successResponse);
                } else if (response.errorBody() != null){
                    Error errorResponse = new Gson().fromJson(response.errorBody().toString(), Error.class);
                    errorCallback.execute(errorResponse);
                } else {
                    Error error = new Error("Unknown Error", -1);
                    errorCallback.execute(error);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("getNowPlayingMovies", t.getLocalizedMessage());
                Error error = new Error(t.getMessage(), -1);
                errorCallback.execute(error);
            }
        });
    }

    public void getSimilarMovies(int pageNumber, int movieId, Callback<Success> successCallback, Callback<Error> errorCallback) {

        Call<JsonObject> call = mApiService.getSimilarMovies(movieId, Constants.API_KEY, pageNumber, Constants.LANGUAGE);
        call.enqueue(new retrofit2.Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.isSuccessful() && response.body() != null) {
                    Success successResponse = new Gson().fromJson(response.body().toString(), Success.class);
                    successCallback.execute(successResponse);
                } else if (response.errorBody() != null){
                    Error errorResponse = new Gson().fromJson(response.errorBody().toString(), Error.class);
                    errorCallback.execute(errorResponse);
                } else {
                    Error error = new Error("Unknown Error", -1);
                    errorCallback.execute(error);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                Log.d("getNowPlayingMovies", t.getLocalizedMessage());
                Error error = new Error(t.getMessage(), -1);
                errorCallback.execute(error);
            }
        });
    }
}