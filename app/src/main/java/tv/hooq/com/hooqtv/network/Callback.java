package tv.hooq.com.hooqtv.network;

public interface Callback<T> {
    void execute(T result);
}