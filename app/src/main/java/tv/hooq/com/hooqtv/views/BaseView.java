package tv.hooq.com.hooqtv.views;

import android.support.v4.app.FragmentActivity;

/**
 * BaseView.
 */
public interface BaseView extends UserMessageView {

    /**
     * @return current container activity from view for purpose of launching new activity
     */
    FragmentActivity getActivity();
}