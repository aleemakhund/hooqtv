package tv.hooq.com.hooqtv.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.views.BaseView;
import tv.hooq.com.hooqtv.views.UserMessageView;

public abstract class BaseFragment extends Fragment implements BaseView {

    protected UserMessageView userMessageView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getView() != null) {
            userMessageView = (UserMessageView) getView().findViewById(R.id.view_user_message);
        }
    }

    /**
     * User message view.
     */
    @Override
    public void hideUserMessage() {
        if (isAdded() && userMessageView != null) {
            userMessageView.hideUserMessage();
        }
    }

    @Override
    public void showUserMessageLoading(CharSequence message) {
        if (isAdded() && userMessageView != null) {
            userMessageView.showUserMessageLoading(message);
        }
    }

    @Override
    public void showUserMessageSuccess(CharSequence message, boolean autoHide) {
        if (isAdded() && userMessageView != null) {
            userMessageView.showUserMessageSuccess(message, autoHide);
        }
    }

    @Override
    public void showUserMessageError(CharSequence message, boolean autoHide) {
        if (isAdded() && userMessageView != null) {
            userMessageView.showUserMessageError(message, autoHide);
        }
    }

    @Override
    public void showUserMessageRetry(CharSequence message, View.OnClickListener listener) {
        if (isAdded() && userMessageView != null) {
            userMessageView.showUserMessageRetry(message, listener);
        }
    }

}