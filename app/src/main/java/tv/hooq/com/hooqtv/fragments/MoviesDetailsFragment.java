package tv.hooq.com.hooqtv.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tv.hooq.com.hooqtv.Constants;
import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.activities.MovieDetailActivity;
import tv.hooq.com.hooqtv.adapters.MoviesAdapter;
import tv.hooq.com.hooqtv.models.Movie;
import tv.hooq.com.hooqtv.network.RetrofitManager;
import tv.hooq.com.hooqtv.presenters.SimilarMoviesPresenter;
import tv.hooq.com.hooqtv.views.ISimilarMoviesView;
import tv.hooq.com.hooqtv.views.LoadMoreRecyclerView;
import tv.hooq.com.hooqtv.views.MovieClickListener;
import tv.hooq.com.hooqtv.views.MovieDetailCustomView;

public class MoviesDetailsFragment extends BaseFragment implements ISimilarMoviesView {

    private MovieDetailCustomView mMovieDetailCustomView;
    private Movie mMovie;

    private LoadMoreRecyclerView mLoadMoreRecyclerView;
    private SimilarMoviesPresenter mPresenter;
    private MoviesAdapter mAdapter;

    public static MoviesDetailsFragment newInstance() {
        return new MoviesDetailsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {

            if (bundle.containsKey(Constants.ARG_MOVIE_DETAIL)) {
                mMovie = (Movie) bundle.getSerializable(Constants.ARG_MOVIE_DETAIL);
            }
        }

        if (mMovie != null) {
            mPresenter = new SimilarMoviesPresenter(RetrofitManager.getInstance(), mMovie.getId());
            mPresenter.attachView(this);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movies_detail, container, false);
        mMovieDetailCustomView = rootView.findViewById(R.id.view_movie_detail);
        mLoadMoreRecyclerView = rootView.findViewById(R.id.rv_movies_list);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //set UI
        if (mMovie != null) {
            setSelectedMovieDetail();

            mLoadMoreRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            mLoadMoreRecyclerView.setAddLoadMoreView(true);

            //presenter init
            if (mPresenter != null) {
                mPresenter.init();
            }
        }
    }

    private void setSelectedMovieDetail() {
        mMovieDetailCustomView.setMovieDetail(mMovie);
    }

    @Override
    public void setSimilarMovies(List<Movie> movies) {
        if (movies != null && !movies.isEmpty() && mLoadMoreRecyclerView != null) {
            if (mAdapter == null) {
                mAdapter = new MoviesAdapter(movies, moviePosterClickListener);
                mLoadMoreRecyclerView.setAdapter(mAdapter);
                DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                        LinearLayoutManager.HORIZONTAL);
                mLoadMoreRecyclerView.addItemDecoration(dividerItemDecoration);
            } else {
                mAdapter.updateList(movies);
            }

            mLoadMoreRecyclerView.setVisibility(View.VISIBLE);
            mLoadMoreRecyclerView.onLoadingMoreFinish();
        }
    }

    private MovieClickListener moviePosterClickListener = new MovieClickListener() {
        @Override
        public void onPosterClick(Movie movie) {
            MovieDetailActivity.startActivity(getContext(), movie);
        }
    };

    @Override
    public void setLoadMoreListener(LoadMoreRecyclerView.LoadMoreListener loadMoreListener) {
        mLoadMoreRecyclerView.enableAutoLoadMore(loadMoreListener);
    }

    @Override
    public void disableLoadMore() {
        if (mLoadMoreRecyclerView == null) {
            Log.i("MoviesListFragment", "loadMoreRecyclerView == null");
            return;
        }
        mLoadMoreRecyclerView.disableAutoLoadMore();
    }

    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }

}
