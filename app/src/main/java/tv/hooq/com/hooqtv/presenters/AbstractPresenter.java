package tv.hooq.com.hooqtv.presenters;

import android.support.v4.app.FragmentActivity;

import java.lang.ref.WeakReference;

import tv.hooq.com.hooqtv.views.BaseView;

/**
 * AbstractPresenter.
 *
 * @param <V> The view that extends BaseView
 */
public abstract class AbstractPresenter<V extends BaseView> implements BasePresenter<V> {

    private WeakReference<V> view;


    @Override
    public void attachView(V view) {

        //attach view
        this.view = new WeakReference<>(view);

    }

    @Override
    public void detachView() {

        //detach view
        if (view != null) {
            view.clear();
            view = null;
        }
    }

    /**
     * @return view attached to this presenter
     */
    protected V getView() {
        return view == null ? null : view.get();
    }

    /**
     * @return activity attached to view
     */
    protected FragmentActivity getActivity() {
        return getView() != null ? getView().getActivity() : null;
    }

    /**
     * @return true if view is valid
     */
    protected boolean isValidView() {
        return getView() != null;
    }

    /**
     * @return true if activity is valid
     */
    protected boolean isValidActivity() {
        return getView() != null && getView().getActivity() != null && !getView().getActivity().isFinishing();
    }
}

