package tv.hooq.com.hooqtv.presenters;

import tv.hooq.com.hooqtv.views.BaseView;

/**
 * BasePresenter.
 *
 * @param <V> The view that extends BaseView
 */
public interface BasePresenter<V extends BaseView> {

    /**
     * @param view attach the view
     */
    void attachView(V view);

    /**
     * Detach the view.
     */
    void detachView();

    /**
     * Initialize the presenter.
     */
    void init();
}
