package tv.hooq.com.hooqtv.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.activities.MovieDetailActivity;
import tv.hooq.com.hooqtv.adapters.MoviesAdapter;
import tv.hooq.com.hooqtv.decorations.GridSpacingItemDecoration;
import tv.hooq.com.hooqtv.models.Movie;
import tv.hooq.com.hooqtv.network.RetrofitManager;
import tv.hooq.com.hooqtv.presenters.MoviesListPresenter;
import tv.hooq.com.hooqtv.views.IMoviesListView;
import tv.hooq.com.hooqtv.views.LoadMoreRecyclerView;
import tv.hooq.com.hooqtv.views.MovieClickListener;
import tv.hooq.com.hooqtv.views.NoContentView;

public class MoviesListFragment extends BaseFragment implements IMoviesListView {

    private NoContentView mNoContentView;
    private LoadMoreRecyclerView mLoadMoreRecyclerView;
    private MoviesListPresenter mPresenter;
    private MoviesAdapter mAdapter;

    public static MoviesListFragment newInstance() {
        return new MoviesListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPresenter = new MoviesListPresenter(RetrofitManager.getInstance());
        mPresenter.attachView(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_movies_list, container, false);
        mNoContentView = (NoContentView) rootView.findViewById(R.id.view_no_content);
        mLoadMoreRecyclerView = (LoadMoreRecyclerView) rootView.findViewById(R.id.rv_movies_list);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int spanCount = 2;
        mLoadMoreRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), spanCount));
        mLoadMoreRecyclerView.setAddLoadMoreView(true);
        mLoadMoreRecyclerView.addItemDecoration(new GridSpacingItemDecoration(spanCount, 20, true));
        //presenter init
        mPresenter.init();
    }

    @Override
    public void setData(List<Movie> data) {

        if (data != null && !data.isEmpty() && mLoadMoreRecyclerView != null) {
            if (mAdapter == null) {
                mAdapter = new MoviesAdapter(data, moviePosterClickListener);
                mLoadMoreRecyclerView.setAdapter(mAdapter);
            } else {
                mAdapter.updateList(data);
            }

            mLoadMoreRecyclerView.setVisibility(View.VISIBLE);
            mNoContentView.setVisibility(View.GONE);
            mLoadMoreRecyclerView.onLoadingMoreFinish();
        } else {
            if (mNoContentView != null) {
                mNoContentView.setText(getString(R.string.no_results_found));
            }
        }
    }

    @Override
    public void setLoadMoreListener(LoadMoreRecyclerView.LoadMoreListener loadMoreListener) {
        mLoadMoreRecyclerView.enableAutoLoadMore(loadMoreListener);
    }

    @Override
    public void disableLoadMore() {
        if (mLoadMoreRecyclerView == null) {
            Log.i("MoviesListFragment", "loadMoreRecyclerView == null");
            return;
        }
        mLoadMoreRecyclerView.disableAutoLoadMore();
    }

    private MovieClickListener moviePosterClickListener = new MovieClickListener() {
        @Override
        public void onPosterClick(Movie movie) {
            MovieDetailActivity.startActivity(getContext(), movie);
        }
    };

    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.detachView();
        }
        super.onDestroy();
    }

}
