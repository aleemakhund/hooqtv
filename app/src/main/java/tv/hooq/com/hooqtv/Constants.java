package tv.hooq.com.hooqtv;

public class Constants {

    public final static String BASE_URL = "https://api.themoviedb.org/3/";
    public final static String API_KEY = "7ce61925063399d263fbb0a68f373a90";
    public final static String LANGUAGE = "en-US";
    public final static String IMAGES_PATH = "https://image.tmdb.org/t/p";
    public final static String W500 = "/w500";

    //params
    public static final String ARG_MOVIE_DETAIL = "arg_movie_detail";
}
