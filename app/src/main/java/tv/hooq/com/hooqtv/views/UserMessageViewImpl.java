package tv.hooq.com.hooqtv.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import tv.hooq.com.hooqtv.R;

/**
 * UserMessageView.
 */
public class UserMessageViewImpl extends RelativeLayout implements UserMessageView {

    private TextView mMessageTextView;
    private ProgressBar mProgressBar;
    private ImageView mRefreshIcon;

    private Handler mHandler;
    private boolean mIsAnimatingToHide = false;
    private boolean mIsAnimatingToShow = false;
    private boolean mIsLoadingMessage = false;

    private ViewPropertyAnimator mAnimator;

    private static final int THREE_SECONDS_DELAY = 3000;
    private static final int FADE_IN_ANIMATION_DURATION = 200;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in.
     */
    public UserMessageViewImpl(Context context) {
        super(context);
        init();
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file.
     *
     * @param context The Context the view is running in.
     * @param attrs   The attributes of the XML tag that is inflating the view.
     */
    public UserMessageViewImpl(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating.
     *
     * @param context      The Context the view is running in.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     */
    public UserMessageViewImpl(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating.
     *
     * @param context      The Context the view is running in.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     * @param defStyleRes  A resource identifier of a style resource that
     *                     supplies default values for the view, used only if
     *                     defStyleAttr is 0 or can not be found in the theme. Can be 0
     *                     to not look for defaults.
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UserMessageViewImpl(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.view_user_message, this);

        mMessageTextView = (TextView) v.findViewById(R.id.tv_user_message);
        mProgressBar = (ProgressBar) v.findViewById(R.id.pb_user_message);
        mRefreshIcon = (ImageView) v.findViewById(R.id.iv_user_message_refresh);

        mHandler = new Handler();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        if (!isInEditMode()) {
            setVisibility(INVISIBLE);
        }
    }

    @Override
    public synchronized void hideUserMessage() {
        hideMessage(THREE_SECONDS_DELAY);
    }

    private synchronized void hideMessage(int duration) {

        //end show if in process
        if (mIsAnimatingToShow && mAnimator != null) {
            mAnimator.cancel();
            endShowAnimation();
        }

        if (isShown() && mIsLoadingMessage && !mIsAnimatingToHide) {
            animate().alpha(0).setDuration(FADE_IN_ANIMATION_DURATION).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mIsAnimatingToHide = true;
                    mIsAnimatingToShow = false;
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    endHideAnimation();
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            }).start();

        } else if (isShown() && !mIsAnimatingToHide) {
            mHandler.post(() -> {
                mAnimator = animate()
                        .translationY(-getHeight())
                        .setDuration(duration)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                mIsAnimatingToHide = true;
                                mIsAnimatingToShow = false;
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                endHideAnimation();
                            }
                        });
                mAnimator.start();
                setOnClickListener(null);
            });
        }
    }

    private synchronized void endHideAnimation() {
        setVisibility(INVISIBLE);
        setTranslationY(-getHeight());
        mIsAnimatingToHide = false;
        mAnimator = null;
    }

    @Override
    public void showUserMessageLoading(CharSequence message) {
        mHandler.post(() -> {
            if (!isShown() || !mIsLoadingMessage) {
                mProgressBar.setVisibility(VISIBLE);
                mRefreshIcon.setVisibility(INVISIBLE);
                mMessageTextView.setText(message);
                setVisibility(VISIBLE);
                if (getAlpha() > 0) {
                    setAlpha(0);
                }
                setTranslationY(0);
                mIsLoadingMessage = true;
                setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black_75));
                animate().alpha(1).setDuration(FADE_IN_ANIMATION_DURATION).start();
            }
        });
    }

    @Override
    public void showUserMessageSuccess(CharSequence message, final boolean autoHide) {
        mHandler.post(() -> {
            mProgressBar.setVisibility(GONE);
            mRefreshIcon.setVisibility(INVISIBLE);
            mIsLoadingMessage = false;
            if (getAlpha() < 1) {
                setAlpha(1);
            }
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.success));
            showMessage(message, autoHide, THREE_SECONDS_DELAY);
        });
    }

    @Override
    public void showUserMessageError(CharSequence message, final boolean autoHide) {
        mHandler.post(() -> {
            mProgressBar.setVisibility(GONE);
            mRefreshIcon.setVisibility(INVISIBLE);
            if (getAlpha() < 1) {
                setAlpha(1);
            }
            mIsLoadingMessage = false;
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.error));
            showMessage(message, autoHide, THREE_SECONDS_DELAY);
        });
    }

    @Override
    public void showUserMessageRetry(CharSequence message, final OnClickListener listener) {
        mHandler.post(() -> {
            mProgressBar.setVisibility(GONE);
            mRefreshIcon.setVisibility(VISIBLE);
            mIsLoadingMessage = false;
            if (getAlpha() < 1) {
                setAlpha(1);
            }
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.black_75));
            showMessage(message, false, THREE_SECONDS_DELAY);
            setOnClickListener(listener);
        });
    }

    private synchronized void showMessage(CharSequence message, boolean autoHide, int duration) {

        //end hide if in process
        if (mIsAnimatingToHide && mAnimator != null) {
            mAnimator.cancel();
            endHideAnimation();
        }

        if (!isShown() && !mIsAnimatingToShow) {

            /**
             * set view invisible, set text, invalidate then listen to VTO
             * to get the correct height using getHeight()
             */
            setVisibility(INVISIBLE);
            mMessageTextView.setText(message);

            ViewTreeObserver vto = mMessageTextView.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    mMessageTextView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    setTranslationY(-getHeight());
                    setVisibility(VISIBLE);
                    mAnimator = animate()
                            .translationY(0)
                            .setDuration(duration)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationStart(Animator animation) {
                                    mIsAnimatingToShow = true;
                                    mIsAnimatingToHide = false;
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    endShowAnimation();
                                }
                            });
                    mAnimator.start();
                }
            });
            invalidate();
        } else {
            mMessageTextView.setText(message);
        }

        if (autoHide) {
            mHandler.postDelayed(() -> hideMessage(duration), THREE_SECONDS_DELAY);
        }
    }

    private synchronized void endShowAnimation() {
        setTranslationY(0);
        mIsAnimatingToShow = false;
        mAnimator = null;
    }
}
