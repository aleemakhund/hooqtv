package tv.hooq.com.hooqtv;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

public class ImageLoader {

    public static void loadImage(String url, ImageView imageView, boolean isFit) {

        RequestCreator requestCreator = Picasso.get().load(url);

        if (isFit) {
            requestCreator.fit();
        }

        if (imageView != null) {
            requestCreator.into(imageView);
        }

    }
}
