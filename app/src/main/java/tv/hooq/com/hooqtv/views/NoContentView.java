package tv.hooq.com.hooqtv.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import tv.hooq.com.hooqtv.R;

/**
 * NoContentView simple LinearLayout containing WF Logo and text.
 */
public class NoContentView extends LinearLayout {

    private TextView textView;
    private ImageView imageView;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in.
     */
    public NoContentView(Context context) {
        this(context, null);
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file.
     *
     * @param context The Context the view is running in.
     * @param attrs   The attributes of the XML tag that is inflating the view.
     */
    public NoContentView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating.
     *
     * @param context      The Context the view is running in.
     * @param attrs        The attributes of the XML tag that is inflating the view.
     * @param defStyleAttr An attribute in the current theme that contains a
     *                     reference to a style resource that supplies default values for
     *                     the view. Can be 0 to not look for defaults.
     */
    public NoContentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        View v = LayoutInflater.from(getContext()).inflate(R.layout.view_no_content, this, true);
        textView = (TextView) v.findViewById(R.id.tv_no_content);
        imageView = (ImageView) v.findViewById(R.id.iv_no_content);

        //Visibility of a View is Hidden at start
        setVisibility(GONE);
    }

    /**
     * Sets title of the item using string resource.
     *
     * @param resId to be set
     */
    public void setText(int resId) {
        textView.setText(resId);
    }

    /**
     * Sets title of the item.
     *
     * @param title to be set
     */
    public void setText(CharSequence title) {
        textView.setText(title);
    }

    /**
     * Sets image of the item using image resource.
     *
     * @param resId to be set
     */
    public void setImage(int resId) {
        imageView.setImageResource(resId);
    }

    /**
     * Sets image of the item using image Drawable.
     *
     * @param drawable to be set
     */
    public void setImage(Drawable drawable) {
        imageView.setImageDrawable(drawable);
    }
}

