package tv.hooq.com.hooqtv.views;

import android.view.View;

/**
 * UserMessageView interface for UserMessageView.
 */
public interface UserMessageView {

    /**
     * Hides user message view.
     */
    void hideUserMessage();

    /**
     * Shows user message view with provided message.
     *
     * @param message to show
     */
    void showUserMessageLoading(CharSequence message);

    /**
     * Shows user success message view with provided message and success color.
     *
     * @param message  to show
     * @param autoHide true if want to auto hide the message
     */
    void showUserMessageSuccess(CharSequence message, boolean autoHide);

    /**
     * Shows user message view with provided message and error color.
     *
     * @param message  to show
     * @param autoHide true if want to auto hide the message
     */
    void showUserMessageError(CharSequence message, boolean autoHide);

    /**
     * Shows user message view with provided message and retry listener.
     *
     * @param message  to show
     * @param listener for retry
     */
    void showUserMessageRetry(CharSequence message, View.OnClickListener listener);
}