package tv.hooq.com.hooqtv.views;

import tv.hooq.com.hooqtv.models.Movie;

public interface MovieClickListener {
    void onPosterClick(Movie movie);
}