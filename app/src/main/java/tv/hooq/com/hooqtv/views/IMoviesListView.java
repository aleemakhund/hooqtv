package tv.hooq.com.hooqtv.views;

import java.util.List;

import tv.hooq.com.hooqtv.models.Movie;

public interface IMoviesListView extends BaseView {

    /**
     * Set data.
     *
     * @param
     */
    void setData(List<Movie> data);

    /**
     * Sets load more listener.
     *
     * @param loadMoreListener listener
     */
    void setLoadMoreListener(LoadMoreRecyclerView.LoadMoreListener loadMoreListener);

    /**
     * Disables load more from Recycler view.
     */
    void disableLoadMore();
}
