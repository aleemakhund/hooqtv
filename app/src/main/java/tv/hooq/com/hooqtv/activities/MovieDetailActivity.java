package tv.hooq.com.hooqtv.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.fragments.MoviesDetailsFragment;
import tv.hooq.com.hooqtv.models.Movie;
import tv.hooq.com.hooqtv.utils.ActivityUtils;

import static tv.hooq.com.hooqtv.Constants.ARG_MOVIE_DETAIL;

public class MovieDetailActivity extends AppCompatActivity {

    private ActionBar mActionBar;

    public static void startActivity(Context context, Movie movie) {
        Intent intent = new Intent(context, MovieDetailActivity.class);
        intent.putExtra(ARG_MOVIE_DETAIL, movie);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        setUpToolBar();
        addFragment();

    }

    private void addFragment() {

        MoviesDetailsFragment moviesDetailsFragment = (MoviesDetailsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.contentFrame);

        if (moviesDetailsFragment == null) {
            moviesDetailsFragment = MoviesDetailsFragment.newInstance();

            Bundle bundle = new Bundle();
            if (getIntent() != null && getIntent().hasExtra(ARG_MOVIE_DETAIL)) {
                Movie movie = (Movie) getIntent().getSerializableExtra(ARG_MOVIE_DETAIL);
                bundle.putSerializable(ARG_MOVIE_DETAIL, movie);

                //Set title
                setToolbarTitle(movie.getTitle());
            }

            moviesDetailsFragment.setArguments(bundle);

            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(),
                    moviesDetailsFragment, R.id.contentFrame);
        }
    }

    // Set up the toolbar.
    private void setUpToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setDisplayShowHomeEnabled(true);
    }

    private void setToolbarTitle(String title) {
        mActionBar.setTitle(title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
