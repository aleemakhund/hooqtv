package tv.hooq.com.hooqtv.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.lang.ref.WeakReference;

import tv.hooq.com.hooqtv.Constants;
import tv.hooq.com.hooqtv.ImageLoader;
import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.models.Movie;
import tv.hooq.com.hooqtv.views.MovieClickListener;

public class MoviesItemViewHolder extends RecyclerView.ViewHolder {

    private View mView;
    private ImageView ivPoster;
    private TextView tvMovieTitle;

    public MoviesItemViewHolder(@NonNull View itemView) {
        super(itemView);
        mView = itemView;
        ivPoster = itemView.findViewById(R.id.iv_poster);
        tvMovieTitle = itemView.findViewById(R.id.tv_movie_title);
    }

    public void onBind(Movie movie, WeakReference<MovieClickListener> movieClickListenerWeakReference) {

        String url = Constants.IMAGES_PATH  + Constants.W500 + movie.getPosterPath();
        ImageLoader.loadImage(url, ivPoster, true);

        tvMovieTitle.setText(movie.getTitle());

        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieClickListenerWeakReference.get().onPosterClick(movie);
            }
        });
    }
}