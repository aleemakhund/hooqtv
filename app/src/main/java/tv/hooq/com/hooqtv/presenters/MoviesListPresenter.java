package tv.hooq.com.hooqtv.presenters;

import tv.hooq.com.hooqtv.R;
import tv.hooq.com.hooqtv.models.Success;
import tv.hooq.com.hooqtv.network.RetrofitManager;
import tv.hooq.com.hooqtv.views.IMoviesListView;

public class MoviesListPresenter extends AbstractPresenter<IMoviesListView> {

    private RetrofitManager mRetrofitManager;
    private int pageNumber = 1;

    public MoviesListPresenter(RetrofitManager retrofitManager) {
        mRetrofitManager = retrofitManager;
    }

    @Override
    public void init() {

        if (!isValidView()) {
            return;
        }

        getView().setLoadMoreListener(() -> {
            pageNumber++;
            getMoviesListFromApi();
        });

        getMoviesListFromApi();
    }

    private void getMoviesListFromApi() {
        if (isValidView() && pageNumber == 1) {
            getView().showUserMessageLoading(getActivity().getString(R.string.loading));
        }

        mRetrofitManager.getNowPlayingMovies(pageNumber,
                result -> {

                    if (result != null) {
                        updateUI(result);
                    }
                }, result -> updateUI(null));
    }

    private void updateUI(Success result) {

        if (!isValidView()) {
            return;
        }

        if (result == null) {
            pageNumber = pageNumber > 1 ? pageNumber-- : 1;
            getView().showUserMessageRetry(
                    getActivity().getString(R.string.an_error_occurred_please_retry),
                    v -> getMoviesListFromApi());
        } else {

            if (result.getPage() >= result.getTotalPages() && getView() != null) {
                getView().disableLoadMore();
            }

            if (getView() != null) {
                getView().hideUserMessage();
                getView().setData(result.getResults());
            }
        }
    }
}
