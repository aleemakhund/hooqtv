package tv.hooq.com.hooqtv.views;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

/* LoadMoreRecyclerView simple RecyclerView that provides load more ability for continuous
 * scrolling.
 */
public class LoadMoreRecyclerView extends RecyclerView {

    private Boolean loadingMore = false;
    private Boolean addLoadMoreView = true;
    private LoadMoreOnScrollListener loadMoreOnScrollListener;

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in.
     */
    public LoadMoreRecyclerView(Context context) {
        super(context);
    }

    /**
     * Constructor that is called when inflating a view from XML. This is called
     * when a view is being constructed from an XML file, supplying attributes
     * that were specified in the XML file.
     *
     * @param context The Context the view is running in.
     * @param attrs   The attributes of the XML tag that is inflating the view.
     */
    public LoadMoreRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /**
     * Perform inflation from XML and apply a class-specific base style from a
     * theme attribute. This constructor of View allows subclasses to use their
     * own base style when they are inflating.
     *
     * @param context  The Context the view is running in.
     * @param attrs    The attributes of the XML tag that is inflating the view.
     * @param defStyle An attribute in the current theme that contains a
     *                 reference to a style resource that supplies default values for
     *                 the view. Can be 0 to not look for defaults.
     */
    public LoadMoreRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * enable list view auto load more.
     *
     * @param loadMoreListener load more listener
     */
    public void enableAutoLoadMore(LoadMoreListener loadMoreListener) {
        disableAutoLoadMore();
        loadMoreOnScrollListener = new LoadMoreOnScrollListener(loadMoreListener);
        addOnScrollListener(loadMoreOnScrollListener);
        loadingMore = true;
    }

    /**
     * To disable auto loading.
     */
    public void disableAutoLoadMore() {
        if (loadMoreOnScrollListener != null) {
            removeOnScrollListener(loadMoreOnScrollListener);
        }
        onLoadingMoreFinish();
    }

    private class LoadMoreOnScrollListener extends RecyclerView.OnScrollListener {

        private LoadMoreListener mLoadMoreListener;

        public LoadMoreOnScrollListener(LoadMoreListener loadMoreListener) {
            this.mLoadMoreListener = loadMoreListener;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!loadingMore) {
                if (null == getLayoutManager() || null == getAdapter()) {
                    return;
                }
                int mVisibleItemCount = getLayoutManager().getChildCount();
                int mTotalItemCount = getLayoutManager().getItemCount();
                int mFirstVisibleItemPosition = 0;
                if (getLayoutManager() instanceof LinearLayoutManager) {
                    mFirstVisibleItemPosition = ((LinearLayoutManager)
                            getLayoutManager()).findFirstVisibleItemPosition();
                }

                if ((mVisibleItemCount + mFirstVisibleItemPosition) >= mTotalItemCount) {
                    if (null != mLoadMoreListener) {
                        loadingMore = true;
                        mLoadMoreListener.loadMore();
                        if (getAdapter() instanceof LoadMoreRecyclerAdapter && addLoadMoreView) {
                            ((LoadMoreRecyclerAdapter) getAdapter()).addLoadMore();
                            if (getLayoutManager() instanceof GridLayoutManager) {
                                ((GridLayoutManager) getLayoutManager()).setSpanSizeLookup(
                                        new GridLayoutManager.SpanSizeLookup() {
                                            @Override
                                            public int getSpanSize(int position) {
                                                int spanCount = ((GridLayoutManager)
                                                        getLayoutManager()).getSpanCount();
                                                if (position == getAdapter().getItemCount() - 1) {
                                                    return spanCount;
                                                } else {
                                                    return 1;
                                                }
                                            }
                                        });
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Callback for loading finished.
     */
    public void onLoadingMoreFinish() {
        loadingMore = false;
        if (getAdapter() instanceof LoadMoreRecyclerAdapter && addLoadMoreView) {
            if (getLayoutManager() instanceof GridLayoutManager) {
                ((GridLayoutManager) getLayoutManager()).setSpanSizeLookup(
                        new GridLayoutManager.SpanSizeLookup() {
                            @Override
                            public int getSpanSize(int position) {
                                return 1;
                            }
                        });
            }
            ((LoadMoreRecyclerAdapter) getAdapter()).removeLoadMore();
        }
    }

    /**
     * Interface for load more.
     */
    public interface LoadMoreListener {

        /**
         * Callback for load more.
         */
        void loadMore();
    }

    /**
     * @return true if load more is enabled.
     */
    public Boolean getAddLoadMoreView() {
        return addLoadMoreView;
    }

    /**
     * @param mAddLoadMore true if want to enable load more.
     */
    public void setAddLoadMoreView(Boolean mAddLoadMore) {
        this.addLoadMoreView = mAddLoadMore;
    }
}