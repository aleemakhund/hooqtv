package tv.hooq.com.hooqtv;

import android.app.Application;

public class HooqApp extends Application {

    private static HooqApp mInstance;

    public static HooqApp getsInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        //init libs or something if you have
    }
}
